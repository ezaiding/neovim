-- bootstrap lazy.nvim, LazyVim and your plugins
vim.o.termguicolors = true
--vim.cmd("colorscheme catppuccino-mocha")

require("config.lazy")
