local plugins = {
    { -- TODO this is anoter test
        "LazyVim/LazyVim",
        opts = {
            colorscheme = "catppuccin",
            --colorscheme = "brogrammer",
        },
    },
    {
        "williamboman/mason.nvim",
        opts = {
            ensure_installed = {
                "pyright",
                "eslint_d",
                "debugpy",
                "gopls",
            },
        },
    },
    {
        "mfussenegger/nvim-dap-python",
        ft = "python",
        dependencies = { "mfussenegger/nvim-dap", "rcarriga/nvim-dap-ui" },
        config = function(_, opts)
            local path = "~/.local/share/nvim/mason/packages/debugpy/venv/bin/python"
            require("dap-python").setup(path)
        end,
    },
    {
        "rcarriga/nvim-dap-ui",
        depends = { "mfussenegger/nvim-dap" },
        config = function()
            local dap = require("dap")
            local dapui = require("dapui")
            dapui.setup()
            dap.listeners.after.event_initialized["dapui_config"] = function()
                dapui.open()
            end
            -- i commented out the following because i don't want to close the ui when the debugger exits
            -- dap.listeners.before.event_terminated["dapui_config"] = function()
            -- dapui.close()
            -- end
            -- dap.listeners.before.event_exited["dapui_config"] = function()
            -- dapui.close()
            -- end
        end,
    },
    {
        "mfussenegger/nvim-lint",
        opts = {
            -- Event to trigger linters
            events = { "BufWritePost", "BufReadPost", "InsertLeave" },
            linters_by_ft = {
                python = { "pylint" },
                go = { "golangcilint" },
                javascript = { "eslint_d" },
                markdown = { "markdownlint" },
                bash = { "shellcheck" },
                sh = { "shellcheck" },
                -- Use the "*" filetype to run linters on all filetypes.
                -- ['*'] = { 'global linter' },
                -- Use the "_" filetype to run linters on filetypes that don't have other linters configured.
                -- ['_'] = { 'fallback linter' },
            },
            -- LazyVim extension to easily override linter options
            -- or add custom linters.
            ---@type table<string,table>
            linters = {
                -- -- Example of using selene only when a selene.toml file is present
                -- selene = {
                --   -- `condition` is another LazyVim extension that allows you to
                --   -- dynamically enable/disable linters based on the context.
                --   condition = function(ctx)
                --     return vim.fs.find({ "selene.toml" }, { path = ctx.filename, upward = true })[1]
                --   end,
                -- },
            },
        },
    },
    {
        "olexsmir/gopher.nvim",
        ft = "go",
        config = function(_, opts)
            require("gopher").setup(opts)
        end,
        build = function()
            vim.cmd([[silent! GoInstallDeps]])
        end,
    },
    {
        "zbirenbaum/copilot.lua",
        opts = {
            filetypes = {
                python = true,
                go = true,
                javascript = true,
                typescript = true,
                json = true,
                lua = true,
                rust = true,
                ["*"] = false,
            },
        }, -- your options here
    },
}
return plugins
